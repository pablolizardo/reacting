import React from 'react';
import { StyleSheet, Text, View, Dimensions, SafeAreaView, TouchableOpacity } from 'react-native';
import Brick from './src/components/Brick'
import Hud from './src/components/Hud'
import Finger from './src/components/Finger'

import Game from './src/components/Game'
import SecondsBar from './src/components/SecondsBar'
import colors from './src/constants/colors'
import constants from './src/constants/constants'

const { width, height } = Dimensions.get('window')

export default class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      points: 0,
      seconds: 60,
      w: Math.round(Math.random() * 5 + 3), // max 7
      h: Math.round(Math.random() * 5 + 2), // max 12
      difficulty: 7,
      grid: []
    }
  }
  componentDidMount() {
    this.start()
    this.initGrid()
  }
  start() {
    setInterval(() => {
      if (this.state.seconds > 0)
        this.setState({ seconds: this.state.seconds - 1 })
    }, 1000)
  }
  initGrid() {
    rows = []
    col = []
    for (let i = 0; i < this.state.h; i++) {
      for (let j = 0; j < this.state.w; j++)
        col.push(Math.round(Math.random() * this.state.difficulty))
      col = []
      rows.push(col)
    }
    this.setState({ grid: rows })
  }

  change(i, j) {
    this.state.grid[i][j] = --this.state.grid[i][j]
    this.setState({
      grid: this.state.grid,
      points: ++this.state.points
    })
  }
  drawGrid(grid) {
    let rows = []
    for (let i = 0; i < grid.length; i++) {
      for (let j = 0; j < grid[i].length; j++) {
        rows.push(
          <Brick
            key={i.toString().concat(j.toString())}
            onPress={() => this.change(i, j)}
            bgColor={colors[grid[i][j]]}
            size={(width - constants.padding * 3) / grid[i].length}
            disabled={grid[i][j] == 0 || this.state.seconds == 0}
            label={grid[i][j]}
          />
        )
      }
    }
    return rows
  }

  render() {
    return (
        <SafeAreaView style={{ flex: 1, width: '100%', justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
          <Hud max={this.state.max} points={this.state.points} seconds={this.state.seconds} state={this.state.seconds == 0} />
          <Game init={this.drawGrid(this.state.grid)} />
          <SecondsBar seconds={this.state.seconds}/>
          {/* <Finger /> */}
        </SafeAreaView>
    );
  }
}

