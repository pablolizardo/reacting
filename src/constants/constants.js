export default constants = {
    padding : 20,
    margin: 5,
    borderRadius: 10,
    textXl: 48,
    textLg: 36,
    textBase: 24,
    textSm: 16,
    textXs: 12,

}