import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import constants from '../constants/constants';

export default function Brick(props) {
    return (
        <TouchableOpacity
            disabled={props.disabled}
            onPress={props.onPress}
            style={{
                backgroundColor: props.bgColor,
                borderRadius: constants.borderRadius,
                justifyContent: 'center',
                alignItems: 'center',
                width: props.size,
                height: props.size,
                marginVertical: constants.margin,
            }}>
            <Text style={{
                opacity: .1,
                fontSize: constants.textBase
            }}>{props.label}</Text>
        </TouchableOpacity>
    )
}