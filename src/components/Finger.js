import React, { Component } from 'react';
import { Animated, View, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window')
import {
  PanGestureHandler,
  ScrollView,
  State,
} from 'react-native-gesture-handler';

const circleRadius = 30;
export default class Finger extends Component {
  _touchX = new Animated.Value(width / 2 - circleRadius);
  _touchY = new Animated.Value(width / 2 - circleRadius);
  _onPanGestureEvent = Animated.event([{
    nativeEvent: {
      x: this._touchX,
      y: this._touchY,
    }
  }], { useNativeDriver: true });
  render() {
    return (
      <PanGestureHandler
        onPanGestureEvent={this._onPanGestureEvent}>
        <Animated.View style={{
          height: '100%',
          width: '100%',
          position: 'absolute',
          justifyContent: 'center',
          top: 0, bottom: 0, left: 0, right: 0,
          flex: 1
        }}>
          <View
            style={[{
              backgroundColor: '#42a5f5', borderRadius: circleRadius, height: circleRadius * 2, width: circleRadius * 2,
            }, {
              transform: [
                { translateX: this._touchX },
                { translateY: this._touchY }
              ]
            }]}
          />
        </Animated.View>
      </PanGestureHandler>
    );
  }
}