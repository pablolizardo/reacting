import React from 'react';
import { View, Animated, Easing, Text } from 'react-native';
import constants from '../constants/constants';
import colors from '../constants/colors';

export default class SecondsBar extends React.Component {

    componentWillMount() {
        this.animatedWidth = new Animated.Value(100)
    }
    componentDidMount() {
        Animated.timing(this.animatedWidth, {
            toValue: 0,
            duration: 60000,
            easing: Easing.linear
        }).start()
    }
    render() {
        const animWidth = this.animatedWidth
        return (
            <View style={{
                height: constants.margin ,
                backgroundColor: '#f3f3f3',
                width: '95%',
                overflow: 'hidden',
                borderTopLeftRadius: constants.borderRadius,
                borderTopRightRadius: constants.borderRadius,
                position: 'absolute',
                bottom: 0
            }}>
                <Animated.View style={[{
                    height: '100%',
                    backgroundColor: colors[9],
                    position: 'absolute',
                    bottom: 0,
                    width: animWidth.interpolate(
                        {
                            inputRange: [0, 100],
                            outputRange: ['0%', '100%']
                        }
                    )
                }]}>
                </Animated.View>

            </View>
        )
    }
}