import React from 'react';
import { View, Text } from 'react-native';
import constants from '../constants/constants';
import colors from '../constants/colors';

export default function Hud(props) {
    return (
        <View style={{ 
            justifyContent: 'space-between', 
            flexDirection: 'row', 
            position: 'absolute', 
            width: '100%', 
            // backgroundColor: 'red', 
            top: constants.padding ,
            padding: constants.padding
            }}>
            {/* <Text style={{ fontSize: 24 }}>Current Block: {props.max}</Text> */}
            <Text style={{ fontSize: constants.textXl, color: colors[3] }}>{props.points}</Text>
            {/* <Text style={{ fontSize: 24 }}>{(props.state) ? `GAME OVER` : 'PLAYING...'}</Text> */}
            <Text style={{ fontSize: constants.textXl, color: colors[9] }}>{props.seconds}</Text>
          </View>
    )
}