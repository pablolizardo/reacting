import React from 'react';
import { StyleSheet, Text, View, Dimensions, SafeAreaView, TouchableOpacity } from 'react-native';
const { width, height } = Dimensions.get('window')
import constants from './../constants/constants'

export default function Game (props) {
  return <View style={{
    width: width - constants.padding,
    borderRadius: constants.borderRadius,
    backgroundColor: '#fafafa',
    alignContent: 'stretch',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    flexWrap: 'wrap'
  }}>
    {props.init}
  </View>
}